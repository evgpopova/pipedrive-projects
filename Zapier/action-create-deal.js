'use strict';

var Zap = {
    create_deal_pre_write: function(bundle) {

        var first_name = bundle.action_fields_full.first_name;
        var email = bundle.action_fields_full.email;
        var company_name = bundle.action_fields_full.company_name;
        var api_token = bundle.auth_fields.api_token;
        var contact_id = Zap.get_contact(api_token, email, first_name);
        var company_id, data;
        var req = bundle.request;
        if (company_name) {
            company_id = Zap.get_company(api_token, company_name);
            Zap.update_contact(api_token, contact_id, company_id);
            data = {
                title: "new " + email,
                person_id: contact_id,
                org_id: company_id
            };
            req.data = JSON.stringify(data);
            return Zap.fill_headers(req);
        } else {
            data = {
                title: "new " + email,
                person_id: contact_id
            };
            req.data = JSON.stringify(data);
            return Zap.fill_headers(req);
        }
    },

    get_contact: function(api_token, email, first_name) {
        var contact_id = Zap.find_contact(api_token, email);
        if (typeof contact_id != 'undefined') {
            return contact_id;
        } else {
            contact_id = Zap.create_contact(api_token, email, first_name);
            return contact_id;
        }

    },

    get_company: function(api_token, company_name) {
        var company_id = Zap.find_company(api_token, company_name);
        if (typeof company_id != 'undefined') {
            return company_id;
        } else {
            company_id = Zap.create_company(api_token, company_name);
            return company_id;
        }
    },

    find_company: function(api_token, company_name) {
        var url = "https://api.pipedrive.com/v1/organizations/find?term=" + company_name + "&start=0&api_token=" + api_token;
        var res = Zap.make_request(url, 'GET');
        var content = z.JSON.parse(res.content);
        var company_id;
        if (content.success === true) {
            if (content.data !== null) {
                for (var i = 0; i < content.data.length; i++) {
                    if (content.data[i].name === company_name) {
                        company_id = content.data[i].id;
                    }
                }
            }
        } else {
            throw new HaltedException('Something went wrong while searching a company');
        }
        return company_id;
    },

    create_company: function (api_token, company_name) {
        var url = "https://api.pipedrive.com/v1/organizations?api_token=" + api_token;
        var data = {
            name: company_name
        };
        var res = Zap.make_request(url, "POST", data);
        var company_id;
        var content = z.JSON.parse(res.content);
        if (content.success === true) {
            company_id = content.data.id;
            return company_id;
        } else {
            throw new HaltedException('Something went wrong while creating a company');
        }
    },

    find_contact: function (api_token, email) {
        var url = "https://api.pipedrive.com/v1/persons/find?term=" + email +
            "&start=0&search_by_email=1&api_token=" + api_token;
        var res = Zap.make_request(url, 'GET');
        var content = z.JSON.parse(res.content);
        var contact_id;
        if (content.success === true) {
            if (content.data !== null) {
                for (var i = 0; i < content.data.length; i++) {
                    if (content.data[i].email === email) {
                        contact_id = content.data[i].id; // What if there are several person with the same id
                    }
                }
            }
        } else {
            throw new HaltedException('Something went wrong while searching a contact');
        }
        return contact_id;
    },

    create_contact: function(api_token, email, first_name) {
        var url = "https://api.pipedrive.com/v1/persons?api_token=" + api_token;
        var data = {
          name: first_name,
          email: email
        };
        var res = Zap.make_request(url, "POST", data);
        var contact_id;
        var content = z.JSON.parse(res.content);
        if (content.success === true) {
            contact_id = content.data.id;
            return contact_id;
        } else {
            throw new HaltedException('Something went wrong while creating a contact');
        }
    },

    update_contact: function(api_token, contact_id, company_id) {
        var url = "https://api.pipedrive.com/v1/persons/" + contact_id + "?api_token=" + api_token;
        var data = {
            org_id: company_id
        };
        var res = Zap.make_request(url, "PUT", data);
        var content = z.JSON.parse(res.content);
        if (content.success === false) {
            throw new HaltedException('Something went wrong while updating a contact');
        }
    },

    make_request: function(url, method, data) {
        var request = {
          'method': method,
          'url': url,
          'headers': {},
          'data': JSON.stringify(data)
        };
        if (method === "POST" || method === "PUT") {
            request = Zap.fill_headers(request);
        }
        return z.request(request);
    },

    fill_headers: function(request) {
        request.headers['Content-Type'] = 'application/json';
        return request;
    }
};