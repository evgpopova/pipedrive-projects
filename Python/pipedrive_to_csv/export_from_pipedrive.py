import csv
import sys
import requests

api_token = "****"
PIPEDRIVE_URL = "https://api.pipedrive.com/v1/"


def get_contacts(limit=500):
    params = {"start": 0, "limit": limit, "api_token": api_token}
    req = contacts = requests.get(PIPEDRIVE_URL + "persons?", params=params).json()
    if req.get('success') == False:
        return {"error": req.get("error")}
    page = 1
    while req.get('additional_data').get('pagination').get('more_items_in_collection'):
        params['start'] = params['limit'] * page
        params['limit'] = params['limit'] * page + 500
        req = requests.get(PIPEDRIVE_URL + "persons?", params=params).json()
        contacts['data'] = contacts['data'] + req.get('data')
    list_contact = pipedrive_list_to_appr_list(contacts["data"])
    return list_contact


def get_organizations(limit=500):
    params = {"start": 0, "limit": limit, "api_token": api_token}
    req = organizations = requests.get(PIPEDRIVE_URL + "organizations?", params=params).json()
    if req.get('success') == False:
        return {"error": req.get("error")}
    page = 1
    while req.get('additional_data').get('pagination').get('more_items_in_collection'):
        params['start'] = params['limit'] * page
        params['limit'] = params['limit'] * page + 500
        req = requests.get(PIPEDRIVE_URL + "persons?", params=params).json()
        organizations['data'] = organizations['data'] + req.get('data')
    list_organization = pipedrive_list_to_appr_list(organizations["data"])
    return list_organization


def get_deals(limit=500):
    params = {"start": 0, "limit": limit, "api_token": api_token}
    req = deals = requests.get(PIPEDRIVE_URL + "organizations?", params=params).json()
    if req.get('success') == False:
        return {"error": req.get("error")}
    page = 1
    while req.get('additional_data').get('pagination').get('more_items_in_collection'):
        params['start'] = params['limit'] * page
        params['limit'] = params['limit'] * page + 500
        req = requests.get(PIPEDRIVE_URL + "persons?", params=params).json()
        deals['data'] = deals['data'] + req.get('data')
    list_deals = pipedrive_list_to_appr_list(deals["data"])
    return list_deals


def pipedrive_list_to_appr_list(items):
    appr_list = []
    i = 0
    for item in items:
        appr_list.append({})
        for key, value in item.items():
            if isinstance(value, dict):
                for dict_key, dict_value in value.items():
                    info = {(str(key) + "[" + str(dict_key) + "]"): str(dict_value)}
                    appr_list[i].update(info)
            elif isinstance(value, list):
                j = 0
                for list_value in value:
                    if isinstance(list_value, dict):
                        for dict_key, dict_value in list_value.items():
                            info = {(str(key) + "[" + str(j) + "]" + "[" + str(dict_key) + "]"): str(
                                dict_value)}
                            appr_list[i].update(info)
                    j = j + 1
            else:
                info = {(str(key)): str(value)}
                appr_list[i].update(info)
        i = i + 1
    return appr_list


def list_to_csv(name, list):
    keys = set().union(*(d.keys() for d in list))
    with open(name, "w+") as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(list)


def main_func():
    try:
        deals = get_deals()
        if "error" in deals:
            print(deals)
        else:
            list_to_csv("deals.csv", deals)
    except KeyError:
        sys.stdout.write("You haven't pass any import file")

main_func()
