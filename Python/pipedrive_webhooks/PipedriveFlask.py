from flask import Flask, request, abort
import json, csv
import requests
import os

API_TOKEN = "****"

app = Flask(__name__)


@app.route('/deal', methods=['POST'])
def get_webhooks():
    data = json.loads(request.data.decode())
    retry = data['retry']
    person = data['current']['person_id']
    if retry == 0:
        file = deal_to_csv(data)
        send_file(file, person)
    return "OK"


def deal_to_csv(data):
    deal_data = data['current']
    with open('deal.csv', 'w+') as f:
        w = csv.DictWriter(f, deal_data.keys())
        w.writeheader()
        w.writerow(deal_data)
    f.close()
    return f.name


def send_file(file_name, person):
    path = os.path.realpath(file_name)
    file = {'file': (file_name, open(path, 'rb'), 'CONTENT_TYPE')}
    try:
        r = requests.post('https://api.pipedrive.com/v1/files',
                      params={'api_token': API_TOKEN},
                      data={'file_type': 'csv', 'person_id': person},
                          files=file)
        status = r.status_code
    except:
        abort(500)
        raise


if __name__ == '__main__':
    app.run()
