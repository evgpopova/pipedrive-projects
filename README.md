# Pipedrive projects

These are projects related to Pipedrive CRM.

# Python

## Pipedrive to CSV

Is is a simple python script that can help you export contacts/organizations/deals to CSV file.

* Download the script export_from_pipedrive.py and open it;
* Set your *api_token* to the beginning of the file;
* Select what do you need to export. In *main_func()* set the appropriate function: *get_contacts()*, *get_organizations()*, *get_deals()*;
* Then run the script:
```
python export_from_pipedrive.py
```

## Pipedrive webhooks

It is a simple Flask project. Now the app can get webhooks from Pipedrive about
adding a deal and then create an CSV file and attach it to the person of deal.

### Prerequisites

pip

python

### Installing

Install python packages with pip:

```
pip install -r requirements.txt
```

### Testing

For test purposes you can use [ngrok](https://ngrok.com/).

* Run the PipedriveFlask.py:

```
python PipedriveFlask.py
```
* Run ngrok on the same port:

```
./ngrok http <port>
```
* Set webhooks for adding a deal in Pipedrive with endpoint url that will be
shown after running ngrok and add "/deal" in the end of url; (e.g.
*http://5da175d1.ngrok.io/deal*

* Add a deal in Pipedrive and you will see that person will have an CSV file in
attached files.

# Zapier

Scripts placed in Zapier directory are from different custom Zapier apps.
It is scripting that you use in you apps.


